Due to [Microsoft's acquisition of GitHub](http://www.tfir.io/microsoft-acquires-github-for-7-5-billion), this repo will be a mirror of the same repo on [GitLab](https://gitlab.com/julian-heng/sl). Please do not send any issues or pull request on GitHub as they will be ignored.

# sl
sl is a bash script that will print ls in reverse. Inspired by this [bash script](https://gir.st/blog/sl-alt.htm) by Tobias Girstmair. 

## Example
```sh
$ sl -al /
                                              59 latot
         . 7102  12 peS 6904  toor toor 32  x-rx-rxwrd
        .. 7102  12 peS 6904  toor toor 32  x-rx-rxwrd
       nib 30:31 51 raM 6904  toor toor 2   x-rx-rxwrd
      toob 25:20 92 raM 0652  toor toor 3   x-rx-rxwrd
       ved 95:02 72 raM 0823  toor toor 41  x-rx-rxwrd
       cte 95:61 1  rpA 6904  toor toor 311 x-rx-rxwrd
      emoh 7102  12 peS 6904  toor toor 3   x-rx-rxwrd
       bil 20:31 51 raM 6904  toor toor 61  x-rx-rxwrd
dnuof+tsol 7102  61 guA 48361 toor toor 2   ------xwrd
     aidem 7102  61 guA 6904  toor toor 2   x-rx-rxwrd
       tnm 7102  42 peS 6904  toor toor 3   x-rx-rxwrd
       tpo 7102  12 peS 6904  toor toor 5   x-rx-rxwrd
      corp 0791  1  naJ 0     toor toor 981 x-rx-rx-rd
      toor 11:31 92 raM 6904  toor toor 8   ------xwrd
       nur 03:32 1  rpA 069   toor toor 03  x-rx-rxwrd
      nibs 40:31 51 raM 88221 toor toor 2   x-rx-rxwrd
       vrs 7102  61 guA 6904  toor toor 2   x-rx-rxwrd
       sys 23:22 72 raM 0     toor toor 21  x-rx-rx-rd
       pmt 11:00 2  rpA 6904  toor toor 11  twrxwrxwrd
    hsarT. 7102  12 peS 6904  toor toor 3   ------xwrd
  0-hsarT. 7102  12 peS 6904  toor toor 4   ------xwrd
       rsu 84:22 51 beF 6904  toor toor 11  x-rx-rxwrd
       rav 7102  12 peS 6904  toor toor 21  x-rx-rxwrd
```
```sh
$ ls -al /
total 95
drwxr-xr-x  23 root root  4096 Sep 21  2017 .
drwxr-xr-x  23 root root  4096 Sep 21  2017 ..
drwxr-xr-x   2 root root  4096 Mar 15 13:03 bin
drwxr-xr-x   3 root root  2560 Mar 29 02:52 boot
drwxr-xr-x  14 root root  3280 Mar 27 20:59 dev
drwxr-xr-x 113 root root  4096 Apr  1 16:59 etc
drwxr-xr-x   3 root root  4096 Sep 21  2017 home
drwxr-xr-x  16 root root  4096 Mar 15 13:02 lib
drwx------   2 root root 16384 Aug 16  2017 lost+found
drwxr-xr-x   2 root root  4096 Aug 16  2017 media
drwxr-xr-x   3 root root  4096 Sep 24  2017 mnt
drwxr-xr-x   5 root root  4096 Sep 21  2017 opt
dr-xr-xr-x 187 root root     0 Jan  1  1970 proc
drwx------   8 root root  4096 Mar 29 13:11 root
drwxr-xr-x  30 root root   960 Apr  1 23:30 run
drwxr-xr-x   2 root root 12288 Mar 15 13:04 sbin
drwxr-xr-x   2 root root  4096 Aug 16  2017 srv
dr-xr-xr-x  12 root root     0 Mar 27 22:32 sys
drwxrwxrwt  11 root root  4096 Apr  2 00:11 tmp
drwx------   3 root root  4096 Sep 21  2017 .Trash
drwx------   4 root root  4096 Sep 21  2017 .Trash-0
drwxr-xr-x  11 root root  4096 Feb 15 22:48 usr
drwxr-xr-x  12 root root  4096 Sep 21  2017 var
```
