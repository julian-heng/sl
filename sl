#!/usr/bin/env bash

reverse_string() {
    for ((i=${#1}; i>=0; i--)); do
        out="${out}${1:${i}:1}"
    done
    printf "%s" "${out}"
}

main() {

    # Split the output of ls into arrays
    mapfile -t ls_out < <(ls "$@")

    # Find the maximum amount of characters in the output
    for line in "${ls_out[@]}"; do
        line_length="${#line}"
        { ((line_length >= max)) || [[ -z "${max}" ]]; } && \
            max="${line_length}"
    done

    # Reverse the output of ls and put to an array
    for line in "${ls_out[@]}"; do
        enil="$(reverse_string "${line}")"
        sl_out+=("$(printf "%${max}.${max}s" "${enil}")")
    done

    # Print the contents of the array
    if [[ "$*" == *"l"* ]]; then
        printf "%s\\n" "${sl_out[@]}"
    else
        printf "%s" "${sl_out[@]}"
        printf "\\n"
    fi

}

main "$@"
